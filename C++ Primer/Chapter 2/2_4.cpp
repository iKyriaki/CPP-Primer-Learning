#include <iostream>

using namespace std;

int main()
{
    const int bufSize = 512; // Cannot change the value of a const object after
                             // we create it. Const objects must be initialized.
    
    // To share a const object among multiple files, you must define the variable
    // as extern.

    const int ci = 1024;
    const int &r1 = ci;  // ok: both reference and underlying object are const.
    //r1 = 42;             // error: r1 is a reference to const
    //int &r2 = ci;        // error: non const reference to a const object

    // We can bind a reference to const to a nonconst object, a literal, or a
    // more general expression

    int i = 42;
    const int &r2 = i;  // we can bind a const int& to a plain int object
    const int &r3 = 42; // ok: r1 is a reference to const
    const int &r4 = r2 * 2; // ok: r3 is a reference to const
    //int &r4 = r1 * 2;    // error: r4 is a plain, non const reference

    // We may store the address of a const object only in a pointer to const
    const double pi = 3.14; // pi is const; its value may not be changed
    //double *ptr = &pi;      // error: ptr is a plain pointer
    const double *cptr = &pi; // ok: cptr may point to a double that is const
    //*cptr = 42;               // error: cannot assign to *cptr

    // We indicate that a pointer is const by putting the const after the *
    // This indicates that the pointer, not the pointed-to type, is const.
    int errNumb = 0;
    int *const curErr = &errNumb; // curErr will always point to errNumb
    *curErr = 42;
    const double *const pip = &pi; // pip is a const pointer to a const object

    // A constant expression is an expression whose value cannot change and is
    // known at compile time.
    const int max_files = 20;   // max_files is a constant expression
    const int limit = max_files + 1; // limit is a constant expression
    int staff_size = 27;        // staff_size is not a constant expression
    //const int sz = get_size(); // sz is not a constant expression; its value
                                 // isn't known until run time

    // We can verify that a variable is a constant expression with the constexpr
    // keyword. These variables are implicitly const and must be initialized
    // by constant expressions.
    constexpr int mf = 20;  // 20 is a constant expression
    constexpr int limit = mf + 1; // mf + 1 is a constant expression
    //constexpr int sz = size(); // ok only if size is a constexpr function
    
    // Generally, it is a good idea to use constexpr for variables that you intend
    // to use as constant expressions.

    // When we define a pointer in a constexpr declaration, the constexpr refers
    // to the pointer, not what the pointer points to.
    const int *p = nullptr;     // p is a pointer to a const int
    constexpr int *q = nullptr; // q is a const pointer to int

    return 0;
}
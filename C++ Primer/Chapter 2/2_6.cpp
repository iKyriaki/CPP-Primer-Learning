#include <iostream>
#include "SalesData.h"

int main()
{
    Sales_data data1, data2;
    double price = 0.0;       // price per book, used to calculate total revenue
    // code to read into data1 and data2
    cin >> data1.bookNo >> data1.units_sold >> price;
    cin >> data2.bookNo >> data2.units_sold >> price;
    // calculate total revenue from price and units_sold
    data1.revenue = data1.units_sold * price;
    data2.revenue = data2.units_sold * price;
    // code to check whether data1 and data2 have the same ISBN
    if (data1.bookNo == data2.bookNo)
    {
        //      and if so, print the sum of data1 and data2
        unsigned totalCnt = data1.units_sold + data2.units_sold;
        double totalRevenue = data1.revenue + data2.revenue;

        // print: ISBN, total sold, total revenue, average price per book
        cout << data1.bookNo << " " << totalCnt << " " << totalRevenue << " ";
        if (totalCnt != 0)
            cout << totalRevenue / totalCnt << endl;
        else
            cout << "(no sales)" << endl;
        return 0;   // indicate success
    }
    else
    {
        // transactions weren't for the same ISBN
        cerr << "Data must refer to the same ISBN" << endl;
        return -1;  // indicate failure
    }
    return 0;
}
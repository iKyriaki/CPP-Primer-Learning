#include <vector>
#include <string>
#include <iostream>
using std::vector;
using std::string;
using std::cin;
using std::cout;
using std::endl;

int main()
{
    //vector<int> ivec;            // ivec holds objects of type int
    //vector<vector<string>> file; // vector whose elements are vectors

    //vector<string> svec;      // default initialization; svec has no elements
    //vector<int> ivec;         // initially empty
    //// give ivec some values
    //vector<int> ivec2(ivec);  // copy elements of ivec into ivec2
    //vector<int> ivec3 = ivec; // copy elements of ivec into ivec3
    //vector<string> svec(ivec2); // error: svec holds strings, not ints.

    /// We can only supply a list of element values using list initialization
    /// in which the initializers are enclosed in curly braces.
    /// Enclosing them in parentheses will not work.
    //vector<string> articles = { "a", "an", "the" };  // list initialization
    //vector<string> articles2 = ( "a", "an", "the" ); // error

    /// We can also initialize a vector with vec(count, val), where count
    /// is the number of elements in the vector and val is the value for
    /// each of those elements.
    //vector<int> ivec(10, -1);   // ten int elements, each initialized to -1
    //vector<string> svec(10, "hi!"); // ten strings; each element is "hi!"

    /// We can also initialize a vector with just a size, vec(size), where size
    /// is the number of elements in the vector.
    /// If our vector holds objects of a type that we cannot default initialize,
    /// we must supply an initial element value.
    //vector<int> ivec(10);    // ten elements, each initialized to 0
    //vector<string> svec(10); // ten elements, each an empty string

    /// Initializing a vector using parentheses or curly braces changes how
    /// the vector is initialized.
    /// When we use parentheses, as in v1 and v3, we are saying how many elements
    /// the vector will have and their value. In v1's case, the value is default 0.
    /// When we use curly braces, we are saying, if possible, we want to list initialize
    /// the vector.
    //vector<int> v1(10);      // v1 has ten elements with value 0
    //vector<int> v2{ 10 };    // v2 has one element with value 10
    //vector<int> v3(10, 1);   // v3 has ten elements with value 1
    //vector<int> v4{ 10, 1 }; // v4 has two elements with values 10 and 1.

    /// To add elements to a vector, we use the push_back operation.
    //vector<int> v2;     // empty vector
    //for (int i = 0; i != 100; ++i)
    //    v2.push_back(i);    // append sequential integers to v2
    //// at end of loop, v2 has 100 elements, values 0 ... 99

    //// read words from the standard input and store them as elements in a vector
    //string word;
    //vector<string> text;
    //while (cin >> word)
    //    text.push_back(word);

    /// We can access the elements of a vector in the same way we access the elements
    /// of a string: through their position in the vector.
    vector<int> v{ 1,2,3,4,5,6,7,8,9 };
    for (auto &i : v)       // for each element in v (note: i is a reference)
        i *= i;             // square the element value
    for (auto i : v)        // for each element in v
        cout << i << " ";   // print the element
    cout << endl;

    return 0;
}
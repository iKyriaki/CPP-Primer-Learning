#include <string>
#include <vector>
#include <iostream>

using namespace std;

int main()
{
    string s("some string");
    //if (s.begin() != s.end())   // make sure s is not empty
    //{
    //    auto it = s.begin();    // it denotes the first character in s
    //    *it = toupper(*it);     // make that character uppercase
    //}

    /// Because the iterator returned from end does not denote an element,
    /// it may not be incremented or decremented.

    // process characters in s until we run out of characters or we hit a whitespace
    //for (auto it = s.begin(); it != s.end() && !isspace(*it); ++it)
    //    *it = toupper(*it);     // capitalize the current character

    /// If a vector or string is const, we may only use its const_iterator type.
    /// With a nonconst vector or string, we can use either iterator or const_iterator.

    //vector<int>::iterator it;   // it can read and write vector<int> elements
    //string::iterator it2;       // it2 can read and write characters in a string
    //vector<int>::const_iterator it3; // it3 can read but not write elements
    //string::const_iterator it4; // it4 can read but not write elements

    //vector<int> v;
    //const vector<int> cv;
    //auto it1 = v.begin();       // it1 has type vector<int>::iterator
    //auto it2 = cv.begin();      // it2 has type vector<int>::const_iterator

    /// It is usually best to use a const type, such as a const_iterator, when we need
    /// to read but not write to an object. We can do this also using cbegin and cend.

    //auto it3 = v.cbegin();      // it3 has type vector<int>::const_iterator

    vector<string> text{"Hello", "World", "", "I", "Am", "Demetri"};

    // print each line in text up to the first blank line
    // for text, it will only print "Hello world"
    //for (auto it = text.cbegin(); it != text.cend() && !it->empty(); ++it)
    //    cout << *it << endl;

    /// We can perform a binary search using iterators as follows:

    // text must be sorted
    // beg and end will denote the range we're searching
    auto beg = text.begin(), end = text.end();
    auto mid = text.begin() + (end - beg) / 2;  // original midpoint
    string sought;
    cin >> sought;
    // while there are still elements to look at and we haven't found sought
    while (mid != end && *mid != sought)
    {
        if (sought < *mid)  // is the element we want in the first half?
            end = mid;      // if so, adjust the range to ignore the second half
        else                // the element we want is in the second half
            beg = mid + 1;  // start looking with the element just after mid
        mid = beg + (end - beg) / 2; // new midpoint
    }

    return 0;
}
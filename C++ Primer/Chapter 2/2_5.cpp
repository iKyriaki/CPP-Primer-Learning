#include <iostream>

using namespace std;

int main()
{
    // A type alias is a name that is a synonym for another type.
    typedef double wages;       // wages is a synonym for double
    typedef wages base, *p;     // base is a synonym for double, p for double*

    wages hourly, weekly;       // same as double hourly, weekly

    // Another way to define a type alias is an alias declaration
    // using SI = Sales_item;    // SI is a synonym for Sales_item
    // SI item;                  // same as Sales_item item

    // The type pstring is "pointer to char." So const pstring is a constant
    // pointer to char -- not a pointer to const char.
    typedef char *pstring;
    const pstring cstr = 0; // cstr is a constant pointer to char
    const pstring *ps;      // ps is a pointer to a constant pointer to char

    // Unlike type specifiers, such as double, that name a specific type,
    // auto tells the compiler to deduce the type from the initializer.

    // The type of item is deduced from the type of the result of adding val1 and val2
    // auto item = val1 + val2; // item initialized to the result of val1 + val2

    // We can define multiple variables using auto. The initializers for all the variables
    // in the declaration must have consistent types.
    auto i = 0, *p = &i;    // ok: i is int and p is a pointer to int
    //auto sz = 0, pi = 3.14; // error: inconsistent types for sz and pi

    // The type that the compiler infers for auto is not always exactly the same
    // as the initializer's type. When we use a reference, we are really using
    // the object to which the reference refers. When we use a reference as an
    // initializer, the initializer is the corresponding object. The compiler
    // uses that object's type for auto's type deduction
    int j = 0, &r = j;
    auto a = r;     // a is an int (r is an alias for j, which has type int)

    // auto ordinarily ignores top-level consts and keeps low-level consts.
    const int ci = i, &cr = ci;
    auto b = ci;    // b is an int (top-level const in ci is dropped)
    auto c = cr;    // c is an int (cr is an alias for ci whose const is top-level)
    auto d = &i;    // d is an int* (& of an int object is int*)
    auto e = &ci;   // e is const int* (& of a const object is low-level const)

    // If we want the deduced type to have a top-level const, we must say so explicitly.
    const auto f = ci; // deduced type of ci is int; f has type const int.

    // We can also specify that we want a reference to the auto-deduced type.
    auto &g = ci;   // g is a const int& that is bound to ci
    //auto &h = 42; // error: we can't bind a plain reference to a literal
    const auto &j = 42; // ok: we can bind a const reference to a literal

    //// Exercises 2.5.2
    //// 2.33
    //a = 42; // Legal: a is an int.
    //b = 42; // Legal: b is an int. (top-level const is ignored)
    //c = 42; // Legal: c is an int. (top-level const is ignored)
    //d = 42; // Illegal: d is a pointer to int.
    //e = 42; // Illegal: e is a pointer to const int.
    //g = 42; // Illegal: g is an alias for const int ci.

    //// 2.35
    const int k = 42;
    auto l = k; const auto &m = k; auto *n = &k;
    const auto l2 = k, &m2 = k;
    //// End exercise

    // The decltype specifier analyzes an expression to determine its type
    // but does not evaluate the expression
    //decltype(f()) sum = x; // sum has whatever type f returns

    // decltype returns the type of the variable, including top-level const and references.

    const int ck = 0, &cj = ck;
    decltype(ck) x = 0;     // x has type const int
    decltype(cj) y = x;     // y has type const int& and is bound to x
    //decltype(cj) z;       // error: z is a reference and must be initialized

    // decltype is the only context in which a variable defined as a reference
    // is not treated as a synonym for the object to which it refers

    // decltype of a parenthesized variable is always a reference
    //decltype((i)) d;    // error: d is int& and must be initialized
    decltype(i) e;        // ok: e is an (uninitialized) int

    // decltype((variable)) is always a reference type, but decltype(variable)
    // is a reference type only if variable is a reference.

    //// Exercises 2.5.3
    //// 2.36
    int a = 3, b = 4;
    decltype(a) c = a;
    decltype((b)) d = a;
    ++c;
    ++d;

    //// 2.37
    // Assignment is an example of an expression that yields a reference type.
    // The type is a reference to the type of the left-hand operand.
    int a = 3, b = 4;
    decltype(a) c = a;
    decltype(a = b) d = a;

    return 0;
}
#include <iostream>

int main()
{
    // Exercises 2.1.2
    unsigned u = 10, u2 = 42;
    std::cout << u2 - u << std::endl;
    std::cout << u - u2 << std::endl;
    std::cout << std::endl;

    int i = 10, i2 = 42;
    std::cout << i2 - i << std::endl;
    std::cout << i - i2 << std::endl;

    std::cout << std::endl;
    std::cout << i - u << std::endl;
    std::cout << u - i << std::endl;
    // Exercise 2.1.2 End

    /*std::cout << "a really, really long string literal "
        "that spans two lines" << std::endl;*/

    std::cout << '\n';
    std::cout << "\tHi!\n";

    std::cout << "Hi \x4dO\115!\n";
    return 0;
}
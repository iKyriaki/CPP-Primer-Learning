#include <iostream>
#include <string>
#include <initializer_list>
#include "Chapter6.h"

using namespace std;

// factorial of val is val * (val-1) * (val-2) ...
int fact(int val)
{
	int ret = 1;	// local variable to hold the result as we calculate it
	while (val > 1)
		ret *= val--;	// assign ret * val to ret and decrement val
	return ret;
}

/// Local static objects are not destroyed when a function ends,
/// they are destroyed when a program terminates.
size_t count_calls()
{
	static size_t ctr = 0;	// value will persist across calls
	return ++ctr;
}

/// Exercise 6.10
// function to swap the value of two ints
void swap(int *i, int *j)
{
	int temp = 0;
	temp = *i;		// store i in temp
	*i = *j;		// store j in i
	*j = temp;		// store temp in j
}

// function that takes a pointer and sets the pointed-to value to zero
void reset(int *ip)
{
	*ip = 0;	// changes the value of the object to which ip points
	ip = 0;		// changes only the local copy of ip; the argument is unchanged
}

// reset function, taking a reference instead of a pointer
void reset2(int &i)		// i is just another name for the object passed to reset2
{
	i = 0;				// changes the value of the object to which i refers
}

/// When we'd like to avoid copying large types to use in a function
/// we can use a reference to that object as a function parameter.
// compare the length of two strings
bool isShorter(const string &s1, const string &s2)
{
	return s1.size() < s2.size();
}

/// If we want to return more than one value from a function, we can
/// use reference parameters.
// returns the index of the first occurrence of c in s.
// the reference parameter 'occurs' counts how often c occurs.
string::size_type find_char(const string &s, char c, string::size_type &occurs)
{
	auto ret = s.size();	// position of the first occurrence, if any
	occurs = 0;				// set the occurrence count parameter
	for (decltype(ret) i = 0; i != s.size(); ++i)
	{
		if (s[i] == c)
		{
			if (ret == s.size())
			{
				ret = i;	// remember the first occurrence of c
			}
			++occurs;		// increment the occurrence count
		}
	}
	return ret;				// count is returned implicitly in occurs
}

/// Even though we cannot pass an array by value, we can write a parameter that
/// looks like an array
// despite appearances, these three declarations of print are equivalent
// each function has a single parameter of type const int*
//void print(const int*);
//void print(const int[]);
//void print(const int[10]);

/// We can pass a variable that is a reference to an array.
// ok: parameter is a reference to an array; the dimension is part of the type
void print(int(&arr)[10])
{
	for (auto elem : arr)
		cout << elem << endl;
}
/// The parentheses around &arr are necessary. f(int &arr[10]) declares
/// arr as an array of references.

/// A multidimensional array is passed as a pointer to its first element.
/// The size of any subsequent dimensions is part of the element type.
// matrix points to the first element in an array whose elements are arrays of ten ints
void print(int(*matrix)[10], int rowSize) { }
/// The parentheses around *matrix are necessary. 'int *matrix[10]' is an array of ten pointers.
/// 'int (*matrix)[10]' is a pointer to an array of ten ints.

/// An equivalent declaration is:
//void print(int matrix[][10], int rowSize) { }

/// We can write a function that takes an unknown number of arguments by using an
/// initializer_list parameter. The arguments should be of a single type.
/// The elements of an initializer_list are always const values.
void error_msg(initializer_list<string> il)
{
	for (auto beg = il.begin(); beg != il.end(); ++beg)
		cout << *beg << " ";
	cout << endl;
}
/// When we pass a sequence of values to an initializer_list parameter,
/// we must enclose the sequence in curly braces.
// expected and actual are strings
//if (expected != actual)
//	error_msg({ "functionX", expected, actual });
//else
//	error_msg({ "functionX", "okay" });

// Sum elements in a list.
// Exercise 6.27
void sum(initializer_list<int> intList)
{
	int totalSum = 0;
	for (auto beg = intList.begin(); beg != intList.end(); ++beg)
	{
		totalSum += *beg;
	}
	cout << "The total sum is: " << totalSum << endl;
}

int main()
{
	//int j = fact(5);	// j equals 120, i.e., the result of fact(5)
	//cout << "5! is " << j << endl;

	//for (size_t i = 0; i != 10; ++i)
	//	cout << count_calls() << endl;

	//int i = 42;
	//reset(&i);					// changes i but not the address of i
	//cout << "i = " << i << endl;	// prints i = 0;

	//int i = 42;
	//reset2(i);

	//int i = 2;
	//int j = 5;
	//cout << "i = " << i << endl;
	//cout << "j = " << j << endl;
	//swap(&i, &j);
	//cout << "i = " << i << endl;
	//cout << "j = " << j << endl;

	//string s1 = "Happy happy joy joy.";
	//string s2 = "Sad sad, joy joy.";
	//cout << isShorter(s1, s2) << endl;

	/*string s = "Happy happy joy joy.";
	string::size_type ctr = 0;
	auto index = find_char(s, 'p', ctr);*/
	sum({ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
	return 0;
}

void print(const int[])
{
}

/// Pointers to Functions

#include <iostream>
#include <string>

using namespace std;

/// A function pointer is a pointer that denotes a function rather than an object.

// Compares lengths of two strings
// This has the type bool(const string&, const string&).
bool lengthCompare(const string &, const string &);

// pf points to a function returning bool that takes two const string references
// The parentheses around pf are necessary. Without them, we'd be declaring a function
// named pf that returns a bool*.
bool (*pf)(const string &, const string &);	// uninitialized

string::size_type sumLength(const string&, const string&);
bool cstringCompare(const char*, const char*) { return; }

/// When we declare a pointer to an overloaded function, the compiler
/// uses the type of the pointer to determine which function to use.
/// The type of the pointer must match one of the functions exactly.

void ff(int*);
void ff(unsigned int);
void(*pf1)(unsigned int) = ff;	// pf1 points to ff(unsigned)
//void(*pf2)(int) = ff;			// error: no ff with a matching parameter list
//double(*pf3)(int*) = ff;		// error: return type of ff and pf3 don't match.

/// As with arrays, we can write a parameter that looks like a function type,
/// but it will be treated as a pointer.

//// third parameter is a function type and is automatically treated as a pointer
//// to function
//void useBigger(const string &s1, const string &s2,
//	bool pf(const string &, const string &));

/// Type aliases and decltype can help us simplify code that uses function pointers

// Func and Func2 have function type
typedef bool Func(const string&, const string&);
typedef decltype(lengthCompare) Func2;	// equivalent type

// FuncP and FuncP2 has pointer to function type
typedef bool(*FuncP)(const string&, const string&);
typedef decltype(lengthCompare) *FuncP2;

/// Using these, we can redeclare useBigger.
void useBigger(const string&, const string&, Func);
void useBigger(const string&, const string&, FuncP2);

/// We can return a pointer to a function type. We must write the return type
/// as a pointer type. The easiest way to do this is by using a type alias.

using F = int(int*, int);		// F is a function type, not a pointer
using PF = int(*)(int*, int);	// PF is a pointer type

PF f1(int);		// ok: PF is a pointer to function; f1 returns a pointer to function
//F f1(int);	// error: F is a function type; f1 can't return a function

F *f1(int);		// ok: explicitly specify that the return type is a pointer to function

/// We can also simplify declarations of functions that return pointers to function
/// by using a trailing return

auto f1(int) -> int(*)(int*, int);

int main()
{
	/// When we use the name of a function as a value, the function is
	/// automatically converted to a pointer.
	pf = lengthCompare;		// pf now points to the function named lengthCompare
	pf = &lengthCompare;	// equivalent assignment: address-of operator is optional

	/// We can then use pf to call the function to which pf points.
	/// There's no need to dereference the pointer.
	bool b1 = pf("hello", "goodbye");	// calls lengthCompare

	/// There is no conversion between pointers to one function type and pointers
	/// to another function type.
	pf = 0;					// ok: pf points to no function
	//pf = sumLength;		// error: return type differs
	//pf = cstringCompare;	// error: parameter types differ
	pf = lengthCompare;		// ok: function and pointer types match exactly

	// automatically converts the function lengthCompare to a pointer to function
	useBigger("hello", "goodbye", lengthCompare);

	return 0;
}
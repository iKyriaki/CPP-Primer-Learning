/// Features for Specialized Uses

#include <iostream>
#include <string>

using namespace std;

//// return a reference to the shorter of two strings
//const string &shorterString(const string &s1, const string &s2)
//{
//	return s1.size() <= s2.size() ? s1 : s2;
//}

/// We can define a function as inline by putting the keyword 'inline'
/// before the function's return type. The inline specification usually expands
/// the function "in line" at each call. This optimizes small, frequently
/// called, functions.

inline const string &shorterString(const string &s1, const string &s2)
{
	return s1.size() <= s2.size() ? s1 : s2;
}

/// A constexpr function is a function that can be used in a constant expression.
/// The return type and the type of each parameter must be a literal type
/// and there must be exactly one return statement.

constexpr int new_sz() { return 42; }

/// A constexpr function can return a value that is not constant.
// scale(arg) is a constant expression if arg is a constant expression

constexpr size_t scale(size_t cnt) { return new_sz() * cnt; }

int main()
{
	/// When writing a function, we can allow the function to have default
	/// arguments that can always be overwritten by the user.
	/// If we wanted to represent the contents of a window, we could do as so:
	/*typedef string::size_type sz;
	string screen(sz ht = 24, sz wid = 80, char backgrnd = ' ');*/

	constexpr int foo = new_sz();

	int arr[scale(2)];	// ok: scale(2) is a constant expression
	int i = 2;			// i is not a constant expression
	//int a2[scale(i)];	// error: scale(i) is not a constant expression
	
	return 0;
}
#include <iostream>
#include <string>

/* Static members of a class exist outside any object. The account
 * objects will only contain two data members: owner and amount.
 * The interestRate object will be shared throughout all Account
 * objects.
 * Since static members functions are not bound to any one object,
 * they do not have a 'this' pointer and cannot be declared
 * const. */

class Account {
public:
    void calculate() { amount += amount * interestRate; }
    static double rate() { return interestRate; }
    static void rate(double);
private:
    std::string owner;
    double amount;
    static double interestRate;
    static double initRate();
    static constexpr int period = 30;   // period is a const expression
    double daily_tbl[period];
};
// define and initialize a static class member
double Account::interestRate = initRate();

// If an initializer is provided inside the class, the member's
// definition must not specify an initial value.
constexpr int Account::period;

// When we define a static member outside the class, we do not
// repeat the static keyword.
void Account::rate(double newRate) {
    interestRate = newRate;
}

int main() {
    // Even though static members are not part of the objects of its
    // class, we can use an object, reference, or pointer of the class
    // type to access a static member.

    double r;
    Account ac1;
    Account *ac2 = &ac1;
    // equivalent ways to call the static member rate function
    r = ac1.rate();     // through an Account object or reference
    r = ac2->rate();    // through a pointer to an Account object
    return 0;
}

#ifndef SALES_DATA_H 
#define SALES_DATA_H 

#include <iostream>
#include <string>

/* Data abstraction relies on the separation of interface and implementation. A class's interface consists of the operations that
 * class can use. The implementation includes the data members, the functions that constitute the interface, and functions needed to
 * define the class.
 *
 * Encapsulation separates the interface and implementation. Users have access to the former, but not the latter.
 */ 

// An aggregate class gives users direct access to its members and has special initialization syntax.

struct Data {
    int ival;
    std::string s;

    //// We can initialize the data members of an aggregate class by providing a braced list of member initializers.
    // Data val1 = { 0, "Anna" };
};

class Sales_data {
// friend declarations for nonmember Sales_data operations added
friend Sales_data add(const Sales_data&, const Sales_data&);
friend std::ostream &print(std::ostream&, const Sales_data&);
friend std::istream &read(std::istream&, Sales_data&);

public:
    // A constructor that supplies default arguments for all its parameters also defines the default constructor
    //Sales_data(std::string s = ""): bookNo(s) { }
    // nondelegating constructor initializes members from corresponding arguments
    Sales_data(std::string s, unsigned cnt, double rev) :
        bookNo(s), units_sold(cnt), revenue(rev * cnt) { }
    // remaining constructors all delegate to another constructor
    // In other words, the remaining constructors use the nondelegating constructor to set their values.
    // Any code inside a constructor will execute before returning control to the delegating constructor.
    Sales_data() : Sales_data("", 0, 0) {}
    Sales_data(std::string s) : Sales_data(s, 0, 0) {}
    Sales_data(std::istream &is) : Sales_data() { read(is, *this); }
    //Sales_data(std::istream &is) { read(is, *this); }
    // Objects that are const, and references or pointers to const objects, may call only const member functions.
    std::string isbn() const { return bookNo; }
    Sales_data& combine(const Sales_data&);
private:
    double avg_price() const;
    // data members are unchanged
    std::string bookNo;
    unsigned units_sold = 0;
    double revenue = 0.0;
};
// declarations for nonmember parts of the Sales_data interface
Sales_data add(const Sales_data&, const Sales_data&);
std::ostream &print(std::ostream&, const Sales_data&);
std::istream &read(std::istream&, Sales_data&);

// Read data from the given stream into the given object.
std::istream &read(std::istream &is, Sales_data &item) {
    double price = 0;
    is >> item.bookNo >> item.units_sold >> price;
    item.revenue = price * item.units_sold;
    return is;
}

// Prints the contents of the given object on the given stream.
std::ostream &print(std::ostream &os, const Sales_data &item) {
    os << item.isbn() << " " << item.units_sold << " "
       << item.revenue << " " << item.avg_price();
    return os;
}

// Takes two Sales_data objects and returns a new Sales_data representing their sum.
Sales_data add(const Sales_data &lhs, const Sales_data &rhs) {
    Sales_data sum = lhs;   // copy data members from lhs into sum
    sum.combine(rhs);       // add data members from rhs into sum
    return sum;
}

double Sales_data::avg_price() const {
    if (units_sold)
        return revenue/units_sold;
    else
        return 0;
}

Sales_data& Sales_data::combine(const Sales_data &rhs)
{
    units_sold += rhs.units_sold;   // add the numbers of rhs into
    revenue += rhs.revenue;         // the members of 'this' object
    return *this;                   // return the object on which the function was called
}

#endif

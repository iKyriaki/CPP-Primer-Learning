#ifndef PERSON_H
#define PERSON_H

#include <iostream>
#include <string>

struct Person {
    // Defining member functions 
    std::string returnName() const { return personName; } 
    std::string returnAddress() const { return personAddress; }

    // Defining member variables 
    std::string personName;
    std::string personAddress;
};

#endif

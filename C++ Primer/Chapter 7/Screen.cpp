#include "Screen.h"

int main()
{
	/*Screen myScreen;*/
	//// move the cursor to a given position and set that character
	/*myScreen.move(4, 0).set('#');*/
	//Screen myScreen(5, 3, ' ');
	//const Screen blank(5, 3, ' ');
	//myScreen.set('#').display(std::cout);    // calls non-const version
	//blank.display(std::cout);                // calls const version

	Screen::pos ht = 24, wd = 80;      // use the pos type defined by Screen
	Screen scr(ht, wd, ' ');
	Screen *p = &scr;
	char c = scr.get();                // fetches the get member from the object scr
	c = p->get();                      // fetched the get member from the object to which p points
	return 0;
}

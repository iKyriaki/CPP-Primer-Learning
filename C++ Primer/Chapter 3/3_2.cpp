#include <string>
#include <iostream>
#include <cctype>

using std::string;
using std::cin;
using std::cout;
using std::endl;

int main()
{
    //string s1;          // default initialization; s1 is the empty string
    //string s2 = s1;     // s2 is a copy of s1
    //string s3 = "hiya"; // s3 is a copy of the string literal
    //string s4(10, 'c'); // s4 is cccccccccc
    //string s5 = "hiya"; // copy initialization
    //string s6("hiya");  // direct initialization
    //string s7(10, 'c'); // direct initialization, s7 is cccccccccc
    //string s8 = string(10, 'c'); // copy initialization, s8 is cccccccccc

    //string s1, s2;             // empty strings
    //cin >> s1 >> s2;           // read whitespace-separated strings
    //cout << s1 << s2 << endl;  // write s1, s2 to output

    //string word;
    //while (cin >> word)         // read until end-of-file
    //    cout << word << endl;   // write each word followed by a new line

    //string line;
    //// read input a line at a time until end-of-file
    //// getline retains any whitespace whereas the >> operator does not.
    //while (getline(cin, line))
    //    // read input a line at a time and print lines that are longer than 80 characters
    //    if (line.size() > 80)
    //        cout << line << endl;

    //auto len = line.size();     // len has type string::size_type

    //string str("some string");
    //// print the characters in str one character to a line
    //for (auto c : str)      // for every char in str
    //    cout << c << endl;  // print the current character followed by a newline

    //string s("Hello World!!!");
    //// punct_cnt has the same type that s.size returns
    //decltype(s.size()) punct_cnt = 0;
    //// count the number of punctuation marks in s
    //for (auto c : s)        // for every char in s
    //    if (ispunct(c))     // if the character is punctuation
    //        ++punct_cnt;    // increment the punctuation counter
    //cout << punct_cnt
    //     << " punctuation characters in " << s << endl;

    // If we want to change the value of characters in a string, we
    // must define the loop variable as a reference type

    //string s("Hello World!!!");
    //// convert s to uppercase
    //for (auto &c : s)   // for every char in s (note: c is a reference)
    //    c = toupper(c); // c is a reference, so the assignment changes the char in s
    //cout << s << endl;

    /// Using indexing to change certain chars in a string.
    //string s("some string");
    //if (!s.empty())           // make sure there's a character in s[0]
    //    s[0] = toupper(s[0]); // assign a new value to the first character in s
    //cout << s << endl;

    /// Using iteration to change certain chars in a string
    // process characters in s until we run out of characters or we hit a whitespace
    string s("some string");
    for (decltype(s.size()) index = 0; index != s.size() && !isspace(s[index]); ++index)
        s[index] = toupper(s[index]);   // capitalize the current character
    cout << s << endl;

    return 0;
}
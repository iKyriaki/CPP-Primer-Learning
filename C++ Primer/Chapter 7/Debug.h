/* A constexpr constructor can be declared as = default. Otherwise, the body of it must be blank (this way, it
 * both meets the requirements of a constructor, having no return statement, and of a constexpr function, which
 * can only have a return statement. */

#include <iostream>

class Debug
{
public:
    // A constexpr constructor must initialize every data member.
    constexpr Debug (bool b = true): hw(b), io(b), other(b) {}
    constexpr Debug (bool h, bool i, bool o) : hw(h), io(i), other(o) {}
    constexpr bool any() { return hw || io || other; }
    void set_io(bool b) { io = b; }
    void set_hw(bool b) { hw = b; }
    void set_other(bool b) { other = b; }

private:
    bool hw;    // hardware errors other than IO errors
    bool io;    // IO errors
    bool other; // other errors
};
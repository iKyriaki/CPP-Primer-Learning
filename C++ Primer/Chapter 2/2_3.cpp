#include <iostream>

using namespace std;

int main()
{
    int ival  = 1024;
    int *pi   = &ival;   // pi points to an int
    int **ppi = &pi;     // ppi points to a pointer to an int
    int ***pppi = &ppi;  // pppi points to a pointer to a pointer to an int

    cout << "The value of ival\n"
        << "direct value: " << ival << "\n"
        << "indirect value: " << *pi << "\n"
        << "doubly indirect value: " << **ppi << "\n"
        << "triply indirect value: " << ***pppi << "\n"
        << endl;

    int i = 42;
    int *p;      // p is a pointer to an int
    int *&r = p; // r is a reference to the pointer p
    r = &i;      // r refers to a pointer; assigning &i to r makes p point to i
    *r = 0;      // dereferencing r yields i, the object to which p points;
                 // changes i to 0

    return 0;
}
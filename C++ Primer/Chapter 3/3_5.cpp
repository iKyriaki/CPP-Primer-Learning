#include <iostream>
#include <cstring>
#include <string>

using namespace std;

int main()
{
    /// Unlike vectors, arrays have a fixed size. If you don't know
    /// how many elements you need, use a vector.
    /// The size of an array must be a constant expression.

    //unsigned cnt = 42;          // not a constant expression
    //constexpr unsigned sz = 42; // constant expression

    //int arr[10];        // array of ten ints
    //int *parr[sz];      // array of 42 pointers to int
    ////string bad[cnt];         // error: cnt is not a constant expression
    ////string strs[get_size()]; // ok if get_size is constexpr, error otherwise

    /// We cannot use auto to deduce the type of an array from
    /// a list of initializers. Also, there cannot be an array of references

    /// We can list initialize the elements in an array. The size can be omitted
    /// in this case. The list of initializers cannot exceed the specified size.
    //const unsigned sz = 3;
    //int a1[sz] = { 0,1,2 };    // array of three ints with values 0, 1, 2
    //int a2[] = { 0,1,2 };       // array of dimension 3
    //int a3[5] = { 0,1,2 };      // equivalent to a3[] = {0, 1, 2, 0, 0}

    /// With character arrays, it is important to remember that string literals
    /// end with a null character. That character is copied into the array
    /// along with the string literal.
    //char a1[] = { 'C', '+', '+' };  // list initialization, no null
    //char a2[] = { 'C', '+', '+', '\0' };  // list initialization, explicit null
    //char a3[] = "C++";              // null terminator added automatically
    //const char a4[6] = "Daniel";    // error: no space for null!

    /// We cannot initialize an array as a copy of another array.
    /// We cannot assign one array to another.
    //int a[] = { 0,1,2 };    // array of three ints
    //int a2[] = a;           // cannot initialize one array with another

    /// It can be easier to understand array declarations by starting
    /// with the array's name and reading them from the inside out.
    //int *ptrs[10];      // ptrs is an array of ten pointers to int
    ////int &refs[10];      // error: no arrays of references

    //int arr[10] = { 1 };
    //int(*Parray)[10] = &arr;    // Parray points to an array of ten ints
    //int(&arrRef)[10] = arr;     // arrRef refers to an array of ten ints

    /// When we use an array, the compiler converts it to a pointer.
    /// We can obtain a pointer to an array element by taking the address
    /// of that element.
    //string nums[] = { "one", "two", "three" };  // array of strings
    //string *p = &nums[0];                       // p points to the first element in nums

    /// In most places when we use an array, the compiler automatically
    /// points to the first element.
    //string *p2 = nums;  // equivalent to p2 = &nums[0]

    /// Pointers to array elements support the same operators as iterators on
    /// vectors and strings. We can also take the address of the element
    /// one past the last element of an array
    //int arr[] = { 0,1,2,3,4,5,6,7,8,9 };
    //int *p = arr;   // p points to the first element in arr
    //++p;            // p points to arr[1]
    //int *e = &arr[10];

    //for (int *b = arr; b != e; ++b)
    //    cout << *b << endl; // print the elements in arr

	/// Because it is error-prone to compute an off-the-end pointer, the new library
	/// gives us two functions, begin and end, that take an array as an argument.
	//int ia[] = { 0,1,2,3,4,5,6,7,8,9 };		// ia is an array of ten ints
	//int *beg = begin(ia);	// pointer to the first element in ia
	//int *last = end(ia);	// pointer one past the last element in ia
	//// find the first negative element, stopping if we've seen all the elements
	//while (beg != last && *beg >= 0)
	//	++beg;

	/// Pointer Arithmetic
	/// When we add or subtract an int to or from a pointer, the result is a new pointer.
	/// That pointer points to the element ahead of or behind the original pointer.
	//constexpr size_t sz = 5;
	//int arr[sz] = { 1,2,3,4,5 };
	//int *ip = arr;		// equivalent to int *ip = &arr[0]
	//int *ip2 = ip + 4;  // ip2 points to arr[4], the last element in arr

	//// ok: arr is converted to a pointer to its first element; p points one past the end of arr
	//int *p = arr + sz;		// use caution -- do not dereference!
	//int *p2 = arr + 10;		// error: arr has only 5 elements; p2 has undefined value

	/// As with iterators, subtracting two pointers gives us the distance between those pointers.
	/// The two pointers must point to elements in the same array.

	//auto n = end(arr) - begin(arr);	// n is 5, the number of elements in arr

	/// We can use relational operators to compare pointers that point to elements of the array,
	/// or one past the last element in that array.

	//int *b = arr, *e = arr + sz;
	//while (b < e)
	//{
	//	// use *b
	//	cout << *b << endl;
	//	++b;
	//}

	//constexpr size_t sz = 5;
	//int arr[sz] = { 1,2,3,4,5 };
	//int *beg = begin(arr);
	//int *last = end(arr);
	//while (beg < last) {
	//	// change the element beg points to to 0
	//	*beg = 0;
	//	// increment beg
	//	++beg;
	//}

	/// C-Style Strings

	//char ca[] = { 'C', '+', '+' };	// not null terminated
	//cout << strlen(ca) << endl;		// disaster: ca isn't null terminated
	// strlen will continue searching until it finds a null character
	// the result, in this case, is 15 instead of 3.

	/// Using relational or equality operators on C-Style strings compares
	/// pointer values, not the strings themselves.
	//string s1 = "A string example";
	//string s2 = "A different string";
	//// s1 > s2

	//const char ca1[] = "A string example";
	//const char ca2[] = "A different string";
	//// Undefined: compares two unrelated addresses.

	/// To compare two strings, use strcmp instead.
	//if (strcmp(ca1, ca2) < 0)	// same effect as string comparison s1 < s2

    return 0;
}
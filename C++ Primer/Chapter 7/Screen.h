#include <iostream>
#include <string>

class Screen
{
    // Window_mgr members can access the private parts of class Screen
    // The friend functions will be implicitly inline.
    friend class Window_mgr;
    // Alternatively, we could just make the clear method of Window_mgr a friend.
    // Window_mgr::clear must have been declared before class Screen.
    //friend void Window_mgr::clear(ScreenIndex);
    
    // ostream version of storeOn may access the private parts of Screen objects.
    //friend std::ostream& storeOn(std::ostream &, Screen &);
public:
    typedef std::string::size_type pos;
    Screen() = default; // needed because Screen has another constructor
    // cursor initialized to 0 by its in-class initializer
    Screen(pos ht, pos wd, char c): height(ht), width(wd), contents(ht * wd, c) { } 
    // get the character at the cursor
    char get() const { return contents[cursor]; } // implicitly inline
    inline char get(pos ht, pos wd) const;        // explicitly inline
    Screen &move(pos r, pos c);                   // can be made inline later
    Screen &set(char);
    Screen &set(pos, pos, char);
    Screen &display(std::ostream &os) { do_display(os); return *this; }
    const Screen &display(std::ostream &os) const { do_display(os); return *this; }
    void some_member() const;
private:
    // function to do the work of displaying a Screen
    void do_display(std::ostream &os) const { os << contents; }
    mutable size_t access_ctr;  // may change even in a const object
    pos cursor = 0;
    pos height, width = 0;
    std::string contents;
};

inline Screen &Screen::set(char c) {
    contents[cursor] = c;   // set the new value at the current cursor location
    return *this;           // return this object as an lvalue
}

inline Screen &Screen::set(pos r, pos col, char ch) {
    contents[r*width + col] = ch;   // set specified location to given value
    return *this;                   // return this object as an lvalue
}

// we can specify inline on the definition
inline Screen &Screen::move(pos r, pos c) {
    pos row = r * width;    // compute the row location
    cursor = row + c;       // move cursor to the column within that row
    return *this;
}
// declared as inline in the class
char Screen::get(pos r, pos c) const {
    pos row = r * width;        // compute row location
    return contents[row + c];   // return character at the given column
}

void Screen::some_member() const {
    ++access_ctr;   // keep a count of the calls to any member function
}
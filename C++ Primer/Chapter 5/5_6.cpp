/// Try Blocks and Exception Handling

#include "SalesData.h"
#include <iostream>

using namespace std;

int main()
{
	/// A throw expression tells the compiler that we've reached an error
	/// and will terminate the current function and give control to the
	/// appropriate handler. To raise an expection type throw expression_name
	//throw runtime_error("Text");

	/// The general form of a try block is
	// try {
	//   program-statements
	//	} catch (exception-declaration) {
	//	 handler-statements
	//	} catch (exception-declaration) {
	//	 handler-statements
	//	}
	// }

	/// When a catch is selected to handle an exception, the associated
	/// block is executed. When a catch is finished, program execution
	/// continues immediately following the last catch clause.

	/// Exercises 5.6.3
	// Very buggy.
	int num1, num2;		// Define two ints
	cout << "Please enter two numbers. " << endl;
	while (cin >> num1 >> num2) {	// The user enters numbers
		try {
			// Throw error if num2 is 0.
			if (num2 == 0)
				throw runtime_error("Num2 should not be 0.");
		}
		catch (runtime_error err) {
			// Tell the user what the error is.
			cout << err.what() << endl;
			cout << "Try again? Press y or n: ";
			// Give them another chance to enter a number.
			char c;
			cin >> c;
			// Break out of while loop if no input is given
			// or if input is 'n'.
			if (!cin || c == 'n')
				break;
		}
	}
	cout << num1 / num2 << endl;

	return 0;
}